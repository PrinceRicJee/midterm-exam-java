
package number3;

public class test {

    public static void main(String[] args) {
        
        MyDate d1 = new MyDate(2021, 2, 26);
        System.out.println(d1);             
        System.out.println(d1.nextDay());   
        System.out.println(d1.nextDay()); 
        System.out.println(d1.nextMonth()); 
        System.out.println(d1.nextYear());

        System.out.println("*********************************************************************************************************************************************************************************"); 
        
        MyDate d2 = new MyDate(2021, 1, 27);
        System.out.println(d2);               
        System.out.println(d2.previousDay()); 
        System.out.println(d2.previousDay()); 
        System.out.println(d2.previousMonth()); 
        System.out.println(d2.previousYear()); 

        System.out.println("*********************************************************************************************************************************************************************************"); 
       
        MyDate d3 = new MyDate(2021, 2, 28);
        System.out.println(d3.previousYear()); 

    
    }
}
