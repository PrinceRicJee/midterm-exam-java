/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package number2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {
   
    public static void main(String[] args){
        
        Customer c1 = new Customer("James Aldrin Odiong", true, "Silver");
        Customer c2 = new Customer("Victoriano Moya Jr.", false, "Gold");
        Customer c3 = new Customer("Jurick Anthony Baybayanun", true, "Gold");
        
        System.out.println(c1.toString());
        System.out.println(c2.toString());
        System.out.println(c3.toString());

        System.out.println("===============================================================================================================================================================================================");
        Visit v1 = new Visit(c1, new Date());
        System.out.println(v1.toString());

        v1.setProductExpense(1.9);
        v1.setServiceExpense(8.5);
        v1.setProductExpense(6.5);
        System.out.println(v1.toString());
        System.out.println("Total expense made by " + v1.getName() + " = " + v1.getTotalExpense());
          
    }
}
