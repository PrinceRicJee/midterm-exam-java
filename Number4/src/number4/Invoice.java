package number4;

public class Invoice {

    private int id;
    private Customer customer;
    private double amount;

    Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    
    public int getID() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerID() {
        return id;
    }

    public String getCustomerName() {
        return customer.name;
    }

    public int getCustomerDiscount() {
        return customer.discount;
    }

    public double getAmountAfterDiscount() {
        return (amount - (amount * customer.discount/100.0));
    }

    @Override
    public String toString() {
        return "Invoice{" + "id = " + id + ", customer = " + customer + ", amount = " + amount + '}';
    }

}
