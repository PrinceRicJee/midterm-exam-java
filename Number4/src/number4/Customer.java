package number4;

public class Customer {
    
    private int id;
    String name;
    int discount;

    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }
    
    public int getID(){
        return id;
    }
    public String getName(){
        return name;
    }
    public void setDiscount(int discount){
        this.discount = discount;
    }

    public int getDiscount() {
        return discount;
    }
    
    @Override
    public String toString(){
        return name + "\tID = ("+ id +") discount(" + discount + ")";
    }
}
